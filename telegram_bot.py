import telebot
from telebot import types


def telegram_bot():
    bot = telebot.TeleBot('5248008152:AAG1pp7HINme1IQABCAg83xL5dbFGeLjV_M')
    # while True:
    #     bot.send_message(271041226, text='сам тебе пишу')

    @bot.message_handler(commands=['start'])
    def start_message(message):
        # kb = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        # but = types.KeyboardButton(text='Start', request_contact=True)
        # kb.add(but)
        # print(message.contact)
        print(message.chat.id, message.contact, message.from_user)
        bot.send_message(message.chat.id, 'Got it')

    @bot.message_handler(commands=['button'])
    def button_message(message):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        button = types.KeyboardButton('Авторизоваться в боте', request_contact=True)
        markup.add(button)
        bot.send_message(message.chat.id, 'Выберите нужное', reply_markup=markup)

    @bot.message_handler(content_types=['contact'])
    def contact(message):
        pn = message.contact.phone_number[1:]
        print(pn)
        # reports = calculate_report(pn)
        reports_str = 'Ваши отчеты по маркетплейсам на сегодня:\n'
        # for rep in reports:
        #     reports_str += f"{rep['type']} - {rep['name']}\nСумма заказов: {rep['ordered_price']}\n" \
        #                    f"Единиц товара: {rep['ordered_count']}\nКоличество отмен: {rep['cancelled_count']}\n" \
        #                    f"Комиссия и логистика: {rep['comis_and_logis']}\nПрибыль: {rep['profit']}\n\n"
        bot.send_message(message.chat.id, reports_str)
        print(message.contact)

    @bot.message_handler(content_types=['text'])
    def send_text(message):
        print(message.from_user)
        bot.send_message(message.chat.id, "На данный момент я умею лишь показывать отчет за день :)\nА вот и твой отчет:\nОтчет")

    bot.infinity_polling()

if __name__ == '__main__':
    telegram_bot()